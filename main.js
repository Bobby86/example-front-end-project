$(document).ready(function () {

    var divContent = $('.content');  //cashed variable

//    minimize general blocks after the document is ready; add 'flag' attribute to check out block state (minimized or not)
    divContent.each(function() {
        $(this).height($(this).find('img').outerHeight(true)).css('overflowY', 'scroll').data('flag', 1);
    });

//    put up blocks on mouseover for 2 px
    var contentOffsetTop = divContent.offset().top;
    divContent.hover(function() {
        divContent.css('top', contentOffsetTop - contentOffsetTop - 2);
    },function(){
        divContent.css('top', contentOffsetTop);
    });

//    on('click'...) with delegation, min\max blocks
    var contentToggle = function() {
        if ( $(this).data('flag') ) {
            $(this).css('height', 'auto').css('overflowY', 'hidden').data('flag', 0);
        } else {
            $(this).height($(this).find('img').outerHeight(true)).css('overflowY', 'scroll').data('flag', 1);
        }
    };

    $('#content-layout').on('click', '.content', contentToggle);

//  sub-menu manipulation
    var topUlLi = $('#top-ul>li');
    topUlLi.on('click', function(e) {
        $(this).children('ul').toggle(200);
    });

    $('.owls-sub-menu').on('click', 'li', function (e) {
        var thisIndex = $(this).index();
        if (thisIndex === 0) {
            contentToggle.call($('#first-owl'));
        } else if (thisIndex === 1) {
            contentToggle.call($('#second-owl'));
        }   else if (thisIndex === 2) {
            contentToggle.call($('#third-owl'));
        }
    });

//    logo positioning
    var logo = $('.top-menu .logo');
    if ( $(window).width() >= 800) {
        logo.addClass('logo-right');
    }
    $(window).on('resize', function(e) {
        if ( ($(window).width() >= 800) && !(logo.hasClass('logo-right'))) {
            logo.addClass('logo-right');
        } else if ( ($(window).width() < 800) && logo.hasClass('logo-right')) {
            logo.removeClass('logo-right');
        }
    });
}); //end of _ready